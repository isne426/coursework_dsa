#include<iostream>
#include<stack>
#include<queue>
using namespace std;

int main()
{
	stack<int> f_unit;
	stack<int> s_unit;
	stack<int> unix;
	stack<int> uniq;
	queue<int> help;
	queue<int> helpme;																							//declaring stack/queue.
	
	
	int countline,n,num,compare,mode;
	int over = 0;
	int needy = 0;
	int p, q, r, s;
																												//declaring variables.
	cout << "Enter set of numbers for the 1st stack (enter -1 to stop)" << endl; 								// Create the first stack
	do
	{
		cin >> num;
		if (num != -1){
			f_unit.push(num);																						//adding numbers to stack
		}
	} while (num != -1);
	
	cout << endl << "-------------------------------------------------------"<<endl;
																							

	cout << "Enter set of numbers for the 2nd stack (enter -1 to stop)" << endl; 								// Create the second stack
	do
	{
		cin >> num;
		if (num != -1){
			s_unit.push(num);
		}
	} while (num != -1);
	cout << endl << "-------------------------------------------------------"<<endl;

	cout << "Choose operation by enter the following number : plus(+) = 1 minus(-) = 2 multiply(*) = 3" << endl; 				// Choose processing operation.
	cin >> mode;



	if (f_unit.size() > s_unit.size()){
		compare = s_unit.size();
	}
	else {
		compare = f_unit.size();
	}																															//deciding top line.
	
	

	if (mode == 1){ 																												//plus mode.
		while (compare != 0)
		{
			compare--;

			p = f_unit.top();
			
			f_unit.pop();
			q = s_unit.top();
			
			s_unit.pop();
			s = p + q;
			

			if (over != 0){
				over--;
				s = s + 1;
			}

			if (s >= 10){
				r = s - 10;
				unix.push(r);
				over = 1;
			}
			else {
				unix.push(s);
			}
		}

		while (!f_unit.empty()){
			s = f_unit.top();
			f_unit.pop();
		
			if (over != 0){
				over--;
				s = s + 1;
				if (s >= 10){
					unix.push(0);
					over = 1;
				}
			}
			else {
				unix.push(s);
			}
		}
		
		while (!s_unit.empty()){
			s = s_unit.top();
			s_unit.pop();
			if (over != 0){
				over--;
				s = s + 1;
				if (s >= 10){
					unix.push(0);
					over = 1;
				}
			}
			else {
				unix.push(s);
			}
		}

		if (over == 1){
			unix.push(1);
		}

		cout << "The result is = ";
		while (!unix.empty())
		{
			cout << unix.top();
			unix.pop();
		}
	}




	if (mode == 2){  																												//minus mode.
	
		while (compare != 0){
			compare--;

			p = f_unit.top();
			f_unit.pop();
			
			q = s_unit.top();
			
			s_unit.pop();

			if (needy != 0){
				needy--;
				p = p - 1;
				if (p == 0){
					p = 9;
					needy = 1;
				}
			}

			s = p - q;
			//cout << s << endl;
			
			if (s<0){
				r = (10 + p) - q;
				unix.push(r);
				needy = 1;
			}
			else {
				unix.push(s);
			}
		}

		while (!f_unit.empty()){
			s = f_unit.top();
			f_unit.pop();
			
			if (needy != 0){
				needy--;
				s = s - 1;
				if (s == 0)
				{
					s = 9;
					needy = 1;
				}
			}
			unix.push(s);
		}
		
		while (!s_unit.empty()){
			s = s_unit.top();
			s_unit.pop();
			
			if (needy != 0){
				needy--;
				s = s - 1;
				
				if (s == 0){
					s = 9;
					needy = 1;
				}
			}
			s_unit.push(s);
		}

		cout << "The result is = ";
		
		while (!unix.empty()){
			cout << unix.top();
			unix.pop();
		}
	}
	
	
	/*if(mode == 3){																												//multiply mode.
	while (compare != 0){
		compare--;

			p = f_unit.top();
			
			f_unit.pop();
			q = s_unit.top();
			
			s_unit.pop();
			s = p * q;
		
	}
	
	cout << "The result is = ";
		while (!unix.empty())
		{
			cout << unix.top();
			unix.pop();
	
	
	}*/
	
	
	
	
	
	
	return 0;
}
